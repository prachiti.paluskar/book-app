import { SharedDatabaseService } from './../modules/shared/services/shared-database.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  constructor(private sharedDatabaseService:SharedDatabaseService,private router:Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(this.sharedDatabaseService.isLoggedIn === 'true') {
        return true;
      }
      else {
        return this.router.parseUrl('/login');
      }
  }
}
