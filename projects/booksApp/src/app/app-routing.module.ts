import { LoginGuard } from './guards/login.guard';
import { AdminGuard } from './guards/admin.guard';
import { UserLoginComponent } from './modules/shared/components/user-login/user-login.component';
import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'',component:UserLoginComponent,canActivate:[LoginGuard]},
  {path:'admin',loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule),canActivate:[AdminGuard]},     
  {path: 'user', loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule) }
];
@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
