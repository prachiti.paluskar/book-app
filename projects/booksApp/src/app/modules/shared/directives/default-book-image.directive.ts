import { Directive,ElementRef } from '@angular/core';

@Directive({
  selector: 'img[defaultBookImage]',
  host:{
    '(error)': 'errorOnImage()'
  }
})
export class DefaultBookImageDirective {

   /*******************constructor******************/
   constructor( private element:ElementRef ) { }
   /******************Properties********************/
   private defaultImgPath= '../../../../assets/images/sample-book.jpg';
 
   /******************Methods**********************/
   public errorOnImage() {
     this.element.nativeElement.src = this.defaultImgPath;
   }
  }