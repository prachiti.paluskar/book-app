import { ViewUsersComponent } from './../admin/components/view-users/view-users.component';
import { CategoryCardComponent } from './../admin/components/category-card/category-card.component';
import { CategoryListComponent } from './../admin/components/category-list/category-list.component';
import { CategoryContainerComponent } from './../admin/components/category-container/category-container.component';
import { BookCardComponent } from './../admin/components/book-card/book-card.component';
import { TotalBooksComponent } from './../admin/components/total-books/total-books.component';
import { BookListComponent } from './../admin/components/book-list/book-list.component';
import { AdminSidebarComponent } from './../admin/components/admin-sidebar/admin-sidebar.component';
import { BookContainerComponent } from './../admin/components/book-container/book-container.component';
import { RouterModule } from '@angular/router';
import { HeaderBarComponent } from './components/header-bar/header-bar.component';
import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { DefaultBookImageDirective } from './directives/default-book-image.directive';
import { UserLoginComponent } from './components/user-login/user-login.component';
import { ViewBookComponent } from './../admin/components/view-book/view-book.component';

@NgModule({
    declarations: [HeaderBarComponent, PageNotFoundComponent, DefaultBookImageDirective,AdminSidebarComponent,BookCardComponent,TotalBooksComponent,UserLoginComponent,BookListComponent,BookContainerComponent,CategoryCardComponent,CategoryContainerComponent,CategoryListComponent,ViewBookComponent,ViewUsersComponent],
    imports: [ 
      CommonModule,
      RouterModule,
      MaterialModule,
      FormsModule,
    ],
    exports: [HeaderBarComponent,DefaultBookImageDirective,AdminSidebarComponent,BookCardComponent,BookListComponent,BookContainerComponent,CategoryListComponent,CategoryContainerComponent,CategoryCardComponent,ViewBookComponent,ViewUsersComponent]
  })
  export class SharedModule { }