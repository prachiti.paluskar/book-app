import { TestBed } from '@angular/core/testing';

import { SharedDatabaseService } from './shared-database.service';

describe('SharedDatabaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedDatabaseService = TestBed.get(SharedDatabaseService);
    expect(service).toBeTruthy();
  });
});
