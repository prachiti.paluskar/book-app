import { Injectable } from '@angular/core';
import _ from 'lodash'; 
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SharedDatabaseService {
 /***************************************Constructor****************************************************/ 
  constructor(private router:Router) {
    this.bookList = localStorage.getItem('bookList')=== null || localStorage.getItem('bookList')=== undefined  ? []:JSON.parse(localStorage.getItem('bookList'));
    this.categoryList = localStorage.getItem('categoryList')=== null || localStorage.getItem('categoryList')=== undefined  ? []:JSON.parse(localStorage.getItem('categoryList'));
    this.isAdmin = localStorage.getItem('isAdmin')===null || localStorage.getItem('isAdmin') === undefined ? []:JSON.parse(localStorage.getItem('isAdmin'));
    this.isLoggedIn = localStorage.getItem('isLoggedIn')===null || localStorage.getItem('isLoggedIn') === undefined ? []:JSON.parse(localStorage.getItem('isLoggedIn'));
    //this.loginList = localStorage.getItem('loginList')===null || localStorage.getItem('loginList') === undefined ? []:JSON.parse(localStorage.getItem('loginList'));
    console.log('currentUserData',localStorage.getItem('currentUserData'));
    const userFromLocal = localStorage.getItem('currentUserData');
    if(userFromLocal=== null || userFromLocal=== undefined || userFromLocal === 'undefined'){
      this.currentUserData;
     }
     else{
      this.currentUserData = JSON.parse(userFromLocal)
     }
     //this.returnHide=localStorage.getItem('returnHide')===null|| localStorage.getItem('returnHide')===undefined ? []:JSON.parse(localStorage.getItem('returnHide'));
 }
  /************************************Properties**********************************************/ 
  public userVerification:UserVerification[]=[
    {
      email:'admin123@gmail.com',
      password:'admin1234'
    },
    {
      email:'jui@gmail.com',
      password:'jui1234'
    },
    {
      email:'sheetal@gmail.com',
      password:'sheetal1234'
    },
    {
      email:'rupali@gmail.com',
      password:'rupali1234'
    }
  ]
  public currentUserData:UserVerification;
  public isAdmin:string;
  public isLoggedIn: string;
  public bookList: BooksModel[] = [];
  public bookEdit: BooksModel;
  public filteredList: BooksModel[] = [];
  public categoryList: CategoryModel[] = [];
  public booksPresentInCategory={};
  public issueBooksUsers={};
  public returnHide:string;
  
  /***************************************Methods**********************************************/ 

  public getBookList():BooksModel[] {
    this.filteredList=_.cloneDeep(this.bookList);
    return _.cloneDeep(this.bookList);        
  }
  public getCategoryList():CategoryModel[] {
    console.log(this.categoryList);
    return _.cloneDeep(this.categoryList);        
  }
  public getCurrentUserList():UserVerification[] {
    console.log("current-list",this.currentUserData);
    //this.filteredList=_.cloneDeep(this.currentUserData);
    return _.cloneDeep(this.currentUserData);   
  }
  public getUserVerification():UserVerification[] {
    console.log("user-verify",this.userVerification);
    return _.cloneDeep(this.userVerification);
  }
  public addData(data:BooksModel)
  { 
    let bookToAdd=_.cloneDeep(data);
    bookToAdd._id= new Date().getTime().toString(); 
    this.bookList.push(bookToAdd);
    console.log("books in service",this.bookList);
    this.saveBookToLocalStorage();
  }
  public addCategory(data:CategoryModel)
  { 
    let categoryToAdd=_.cloneDeep(data);
    categoryToAdd.catId= new Date().getTime().toString(); 
    this.categoryList.push(categoryToAdd);
    console.log("category in service",this.categoryList);
    this.categoryToLocalStorage();
  }
  public deleteData(id)
  {
    let index=_.findIndex(this.bookList,['_id',id]);
    if(index!==-1) {
    this.bookList.splice(index,1);
    }
    this.saveBookToLocalStorage();
  }
  public deleteCategory(id)
  {
    let index=_.findIndex(this.categoryList,['catId',id]);
    if(index!==-1) {
    this.categoryList.splice(index,1);
    }
    this.categoryToLocalStorage();
  }
  private saveBookToLocalStorage() {
  console.log('save to local',this.bookList)
    localStorage.setItem('bookList',JSON.stringify(this.bookList));
  }
  private saveloginDetails() {
    console.log("save login details",this.isLoggedIn);
    localStorage.setItem('isLoggedIn',JSON.stringify(this.isLoggedIn));
  }
  private saveCurrentUserData() {
    console.log("save currentUserData details",this.currentUserData);
    localStorage.setItem('currentUserData',JSON.stringify(this.currentUserData));
  }
  private saveUserVerification() {
    console.log("save user verify",this.userVerification);
    localStorage.setItem('userVerification',JSON.stringify(this.userVerification));
  }
  private saveRoleDetails() {
    console.log("save role details",this.isAdmin);
    localStorage.setItem('isAdmin',JSON.stringify(this.isAdmin));
  }
  private categoryToLocalStorage() {
    console.log('save to local',this.categoryList)
    localStorage.setItem('categoryList',JSON.stringify(this.categoryList));
  }
  public getBookById(id:string):BooksModel {
    const bookIndex = _.findIndex(this.bookList,['_id',id]);
    if(bookIndex!==-1) {
      return _.cloneDeep(this.bookList[bookIndex]);
    }
    console.log("Matching book found at index:",this.bookList[bookIndex]);
    return null;
  }
  public getCategoryById(id:string):CategoryModel {
    const categoryIndex = _.findIndex(this.categoryList,['catId',id]);
    if(categoryIndex!==-1) {
      return _.cloneDeep(this.categoryList[categoryIndex]);
    }
    console.log("Matching book found at index:",this.categoryList[categoryIndex]);
    return null;
  }
  public update(books) {
    let index=_.findIndex(this.bookList,['_id',books._id]);
    this.bookList[index]=books;
    this.saveBookToLocalStorage();
  }
  public updateCategory(category) {
    let index=_.findIndex(this.categoryList,['catId',category.catId]);
    this.categoryList[index]=category;
    this.categoryToLocalStorage();
  }
  public loginDatas(data:UserVerification) {
    for(let i=0;i<this.userVerification.length;i++) {
      if(_.isEqual(this.userVerification[i=0],data)) {  
          console.log('enter in admin');
          this.isAdmin='admin';
          this.currentUserData=this.userVerification[i];
          console.log("current-user",this.currentUserData);  
          this.saveCurrentUserData();
          this.saveRoleDetails();
          this.login(data);
          this.saveloginDetails();
          this.saveUserVerification(); 
          return this.isAdmin;
      }
      else if(_.isEqual(this.userVerification[i=1],data) || _.isEqual(this.userVerification[i=2],data) || _.isEqual(this.userVerification[i=3],data)) {
          console.log("enter in user");
          this.isAdmin='user';
          this.currentUserData=this.userVerification[i];
          console.log("current-user",this.currentUserData);  
          this.saveCurrentUserData();
          this.saveRoleDetails();
          this.login(data);
          this.saveloginDetails();
          this.saveUserVerification();
          return this.isAdmin;
      }
      else {
          alert("Sorry Not a Valid User");
      }
    }
  }
  public login(data:UserVerification) {
      if (data.email === '' && data.password === '') {
        console.log("data",data);   
        this.isLoggedIn='false';
        console.log("Logged In",this.isLoggedIn);
        this.saveloginDetails();
        this.saveCurrentUserData();
        this.saveUserVerification();
        return this.isLoggedIn;
      }
      else {
        this.isLoggedIn='true';
        console.log("data",data); 
        console.log("hi logout has happened",this.isLoggedIn);   
        this.saveloginDetails();
        this.saveCurrentUserData();
        this.saveUserVerification();
        return this.isLoggedIn;
      }
  }
  public issueBook(id) {
    let index=_.findIndex(this.bookList,['_id',id]);
    console.log("available-book",this.bookList[index].availableBooks); 
      if(index!==-1 && this.bookList[index].totalBooks!==0 && this.bookList[index].issuedBooks<=this.bookList[index].totalBooks)
      { 
        if(!this.bookList[index].bookIssuedBy.includes(this.currentUserData.email)) 
        {
          console.log("before return",this.bookList[index].totalBooks);
          this.bookList[index].issuedBooks++;
          console.log("count",this.bookList[index].issuedBooks);  
          this.bookList[index].bookIssuedBy.push(this.currentUserData.email);
          console.log("bookIssuedBy=",this.bookList[index].bookIssuedBy);
          this.bookList[index].availableBooks=this.bookList[index].totalBooks-this.bookList[index].issuedBooks;
          console.log("after return",this.bookList[index].availableBooks);
          console.log("total Books",this.bookList[index].totalBooks);
          this.saveBookToLocalStorage();
          this.saveUserVerification();
          this.saveCurrentUserData();
        }
        else {
         alert("You cannot issue"+"\t"+this.bookList[index].bookName+" again First you have to return the book");
        }
      }
  }
  public returnBook(id,userId) {
    let index=_.findIndex(this.bookList,['_id',id]);
    let userIndex=_.findIndex(this.currentUserData,['id',userId]);
    console.log("available-book",this.bookList[index].availableBooks); 
    if(index!==-1 && this.bookList[index].totalBooks!==0 && this.bookList[index].issuedBooks<=this.bookList[index].totalBooks)
    {
      if(this.bookList[index].bookIssuedBy.includes(this.currentUserData.email)) 
      {  
        console.log("before issue",this.bookList[index].totalBooks);
        this.bookList[index].issuedBooks--;
        this.bookList[index].bookIssuedBy.splice(userIndex,1);
        this.bookList[index].availableBooks=this.bookList[index].totalBooks+this.bookList[index].issuedBooks;
        console.log("after issue",this.bookList[index].availableBooks);
        console.log("Total books",this.bookList[index].totalBooks);
        this.saveBookToLocalStorage();
        this.saveUserVerification();
        this.saveCurrentUserData();
      }
      else {
        alert("First you have to issue"+"\t"+this.bookList[index].bookName);
      }
    }
  }
   public viewBooksInCategory() {
    for(let i=0;i<this.bookList.length;i++) {
     this.booksPresentInCategory[this.bookList[i]._id]=this.bookList[i].bookName;
     console.log(this.booksPresentInCategory);
     
    }
  }
  public issueBooksByUser() {
    for(let i=0;i<this.userVerification.length;i++) {
      if(this.bookList[i].issuedBooks!==0) {
          this.issueBooksUsers[this.userVerification[i].email]=this.userVerification[i].email;
          console.log("issue book by user",this.issueBooksUsers);
      }
      this.saveUserVerification();
      this.saveCurrentUserData();
      this.saveBookToLocalStorage();
    }
  }
}



