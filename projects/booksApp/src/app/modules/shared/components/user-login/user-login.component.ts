import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { SharedDatabaseService } from '../../../shared/services/shared-database.service';
import _ from 'lodash';

@Component({
  selector: 'user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
/****************************************Constructor*********************************/
  constructor( public sharedDatabaseService:SharedDatabaseService,private router:Router) { }
 /****************************************Properties*********************************/
  public isLoggedIn:string;
  public userVerification:UserVerification;
  public data:UserVerification={
    email:'',
    password:''
  }
  public hide = true;
  /****************************************Methods*********************************/
  ngOnInit() {
    this.isLoggedIn=this.sharedDatabaseService.login(this.data);  
    //this.sharedDatabaseService.getLoginList(); 
  }
  public onLoginSubmit() {
    let userType:string=this.sharedDatabaseService.loginDatas(this.data);
    this.isLoggedIn=this.sharedDatabaseService.login(this.data);
    console.log("log data",this.isLoggedIn);
    this.router.navigate(['/'+userType]);
    console.log("hiii login",userType);   
  }
}


