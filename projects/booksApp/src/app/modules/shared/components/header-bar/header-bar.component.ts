import { SharedDatabaseService } from './../../services/shared-database.service';
import { Component, OnInit } from '@angular/core';
import _ from 'lodash';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit {
  constructor(public sharedDatabaseService:SharedDatabaseService) { }
  
  ngOnInit() {
    
  } 

}
