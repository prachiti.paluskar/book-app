import { Router,ActivatedRoute } from '@angular/router';
import { SharedDatabaseService } from './../../../shared/services/shared-database.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'view-book',
  templateUrl: './view-book.component.html',
  styleUrls: ['./view-book.component.scss']
})
export class ViewBookComponent implements OnInit {

  /********************constructor*************/
  constructor(public sharedDatabaseService:SharedDatabaseService,
    private router:Router,
    private activatedRoute:ActivatedRoute) { }
/********************Properties**************/
    public categoryData:CategoryModel;
    public  bookData: BooksModel[]=[];
    private param='';
 /*******************Methods*****************/ 
    ngOnInit() {
      this.param = this.activatedRoute.snapshot.params.id;
    if(this.param){
      console.log("going to service for getid, existing categoryData", this.param);
      this.categoryData=this.sharedDatabaseService.getCategoryById(this.param);  
      if(this.categoryData===null) {  
        this.router.navigate(['/category']);
      }
    }
    this.bookData=this.sharedDatabaseService.getBookList(); 
  }
}
