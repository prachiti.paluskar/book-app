import { Router,ActivatedRoute } from '@angular/router';
import { SharedDatabaseService } from '../../../shared/services/shared-database.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'add-edit-category',
  templateUrl: './add-edit-category.component.html',
  styleUrls: ['./add-edit-category.component.scss']
})
export class AddEditCategoryComponent implements OnInit {
/********************************constructor********************/
  constructor(private sharedDatabaseService:SharedDatabaseService,
    private router:Router,
    private activatedRoute:ActivatedRoute) { }
/**********************************Properties******************/
    public  categoryData: CategoryModel;
    public  bookData: BooksModel[]=[];
    private defaultCategoryData : CategoryModel = {
      catId:'',
      categoryName:'',
      description:'',
      imageUrl:'', 
      idList:[],
      bookPresent:''
  }
    private param = '';
/*******************************Methods******************/
  ngOnInit() {
    this.param = this.activatedRoute.snapshot.params.id;
    if(this.param){
      this.categoryData=this.sharedDatabaseService.getCategoryById(this.param);  
      if(this.categoryData===null) {  
        this.router.navigate(['/user']);
      }
    }
    else{
      this.categoryData = this.defaultCategoryData;  
    }
    this.bookData=this.sharedDatabaseService.getBookList();
    console.log("category-data",this.categoryData.idList);
  }
  ngOnDestroy() {
    //this.clearEditData();
  }
  onSubmitClick()
  {  
    if(this.param) {
      this.sharedDatabaseService.updateCategory(this.categoryData);
      this.router.navigate(['/admin/category']);
    }
    else {
      this.sharedDatabaseService.addCategory(this.categoryData);
      console.log("form submitted",this.categoryData);
      this.router.navigate(['/admin/category']);

    }
  }
}
