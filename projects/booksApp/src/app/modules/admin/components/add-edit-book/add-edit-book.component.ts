import _ from 'lodash';
import { Component, OnInit, Input } from '@angular/core';
import { SharedDatabaseService } from '../../../shared/services/shared-database.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'add-edit-book',
  templateUrl: './add-edit-book.component.html',
  styleUrls: ['./add-edit-book.component.scss']
})
export class AddEditBookComponent implements OnInit {
/************************constructor********************/
  constructor(
    public sharedDatabaseService: SharedDatabaseService,
    private router:Router,
    private activatedRoute:ActivatedRoute) { }
/*****************************Properties*****************/
  public  bookData: BooksModel;
  public categoryData: CategoryModel[]=[];
  public filteredList: CategoryModel
  private defaultBookData : BooksModel = {
    _id:'',
    bookName:'',
    description:'',
    bookImage:'',
    author:'',
    bookStatus:null,
    catIdList:[],
    issuedBooks:0,
    totalBooks:0,
    availableBooks:1,
    bookIssuedBy:[]
    }
  private param = '';
  public selected='';
  /*************************Methods*********************/
  ngOnInit() {
    this.param = this.activatedRoute.snapshot.params.id;
    if(this.param){
      console.log("going to service for getid, existing bookData", this.param);
      this.bookData=this.sharedDatabaseService.getBookById(this.param);  
      if(this.bookData===null) {  
        this.router.navigate(['/admin/books']);
      }
    }
    else{
      this.bookData = this.defaultBookData;  
    }
    this.categoryData=this.sharedDatabaseService.getCategoryList();
  }
  ngOnDestroy() {
    //this.clearEditData();
  }
  onSubmitClick()
  {  
    if(this.param) {
      this.sharedDatabaseService.update(this.bookData);
      this.router.navigate(['/admin/books']);
    }
    else {
      this.sharedDatabaseService.addData(this.bookData);
      console.log("form submitted",this.bookData);
      this.router.navigate(['/admin/books']);

    }
  }
}
