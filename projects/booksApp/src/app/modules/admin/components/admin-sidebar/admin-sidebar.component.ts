import { Component, OnInit } from '@angular/core';
import { SharedDatabaseService } from './../../../shared/services/shared-database.service';
import { Router } from '@angular/router';
import _ from 'lodash';

@Component({
  selector: 'admin-sidebar',
  templateUrl: './admin-sidebar.component.html',
  styleUrls: ['./admin-sidebar.component.scss'],
  host: {
    class: 'app-root flex layout-column'
  }
})
export class AdminSidebarComponent implements OnInit {

  constructor(private sharedDatabaseService: SharedDatabaseService,private router:Router) { }
  ngOnInit() {

  }

}
