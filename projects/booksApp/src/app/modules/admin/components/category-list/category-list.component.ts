import { SharedDatabaseService } from './../../../shared/services/shared-database.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import _ from 'lodash';

@Component({
  selector: 'category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  /*******************constructor********************************/
  constructor( private sharedDatabaseService: SharedDatabaseService,private router:Router) {
  }
  /***************Properties**************/
  public  categoryList: CategoryModel[] = [];
  public filteredList: CategoryModel[]=[];
  public bookList:BooksModel[]=[];
  public searchCategory="";
  public isAdmin:string;
  public catTotal:number;
  /**************Methods************/
  ngOnInit() {
  
    this.getCategoryList();  
    this.isAdmin=this.sharedDatabaseService.isAdmin;
  }
  private getCategoryList()
  {
    this.categoryList=this.sharedDatabaseService.getCategoryList();
    this.filteredList=_.cloneDeep(this.categoryList);
    console.log('cat-list',this.categoryList)
    
  }
  public deleteCategory(index:number) {
   this.sharedDatabaseService.deleteCategory(this.categoryList[index].catId);
   this.getCategoryList();
  }
  public trackByIndex(index:any):any {
    return index;
  }
  /*public totalBooks(index:number) {
    this.sharedDatabaseService.totalBooks(this.categoryList[index].catId);
    this.getCategoryList();
  }*/
  public eventOnChild(data)
  {
    console.log("event triggered from child",data);
  } 
  public editData(index) {
    this.router.navigate(['admin/catEdit/'+this.categoryList[index].catId]);
  } 
  public viewBooksInCategory(index) {
    this.sharedDatabaseService.viewBooksInCategory();
    this.router.navigate(['/user/view/'+this.categoryList[index].catId]);
  }
  //public booksPresent(index) {
    //this.router.navigate(['admin/booksPresent/'+this.categoryList[index].catId]);
  //}
  public filterList() {
    this.filteredList=this.categoryList.filter((category)=>{
      if(category.categoryName.toLowerCase().indexOf(this.searchCategory.toLowerCase())!==-1)
      {
        return true;
      }
      return false;
    })
  }

}

