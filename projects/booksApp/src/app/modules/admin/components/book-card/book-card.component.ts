import { Component, OnInit,Input,Output,EventEmitter} from '@angular/core';
import { SharedDatabaseService } from '../../../shared/services/shared-database.service';
import { Router } from '@angular/router';

@Component({
  selector: 'book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})
export class BookCardComponent implements OnInit {
/******************************************constructor**********************************/
  constructor(public sharedDatabaseService:SharedDatabaseService,private router:Router) { }
/********************************************Properties********************************/
  public isAdmin:string;
  public currentUserData:LoginModel;
  @Input() book;
  @Input() issue:BooksModel;
  @Input() bookData:BooksModel;
  @Output() onDelete = new EventEmitter();
  @Output() onEdit = new EventEmitter();
  @Output() onIssue = new EventEmitter();
  @Output() onReturn = new EventEmitter();
  @Output() onView = new EventEmitter();
  public returnHide:string;
 /*******************************************Methods*********************************/
  ngOnInit() {
    this.isAdmin=this.sharedDatabaseService.isAdmin;
    this.bookData.availableBooks=this.bookData.totalBooks-this.bookData.issuedBooks;
    this.returnHide=this.sharedDatabaseService.returnHide;
  }
  public editData(index) {
    this.onEdit.emit()
  }
  public deleteData() {
    this.sharedDatabaseService.deleteData(this.bookData._id);
    this.onDelete.emit();
  }
  public issueBook(index) {
    this.onIssue.emit();
  }
  public returnBook(index,userIndex) {
    this.onReturn.emit();
  }
  public viewUsers() {
    this.onView.emit();
  }
}
