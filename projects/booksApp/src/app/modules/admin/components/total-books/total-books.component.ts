import { SharedDatabaseService } from '../../../shared/services/shared-database.service';
import { Component,Input,OnInit} from '@angular/core';
import _ from 'lodash';
import { Router } from '@angular/router';


@Component({
  selector: 'total-books',
  templateUrl: './total-books.component.html',
  styleUrls: ['./total-books.component.scss']
})
export class TotalBooksComponent implements OnInit {
  /***********************************constructor**************************************/
  constructor(private sharedDatabaseService:SharedDatabaseService, private router:Router) { 
  }
  /***********************************Properties**************************************/
  @Input() bookCount;
  @Input() books:BooksModel;
  /**********************************Methods****************************************/
  ngOnInit() {
    console.log("book", this.bookCount);
    
  }
}
