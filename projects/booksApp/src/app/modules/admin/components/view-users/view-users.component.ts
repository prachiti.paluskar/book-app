import { Router,ActivatedRoute } from '@angular/router';
import { SharedDatabaseService } from './../../../shared/services/shared-database.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.scss']
})
export class ViewUsersComponent implements OnInit {

   /********************constructor*************/
   constructor(public sharedDatabaseService:SharedDatabaseService,
    private router:Router,
    private activatedRoute:ActivatedRoute) { }
/********************Properties**************/
    public bookData:BooksModel;
    //public  loginData: LoginModel[]=[];
    public currentUserData:UserVerification[]=[];
    private param='';
 /*******************Methods*****************/ 
    ngOnInit() {
      this.param = this.activatedRoute.snapshot.params.id;
    if(this.param){
      console.log("going to service for getid, existing bookData", this.param);
      this.bookData=this.sharedDatabaseService.getBookById(this.param);  
      if(this.bookData===null) {  
        this.router.navigate(['/books']);
      }
    }
    //this.loginData=this.sharedDatabaseService.getLoginList();
    this.currentUserData=this.sharedDatabaseService.getCurrentUserList();
  }

}
