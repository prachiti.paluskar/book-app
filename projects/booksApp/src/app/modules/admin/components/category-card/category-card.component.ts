import { SharedDatabaseService } from './../../../shared/services/shared-database.service';
import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'category-card',
  templateUrl: './category-card.component.html',
  styleUrls: ['./category-card.component.scss']
})
export class CategoryCardComponent implements OnInit {

  constructor(private sharedDatabaseService:SharedDatabaseService) { }
  public isAdmin:string;
  @Input() categoryData:CategoryModel;
  @Output() onEdit = new EventEmitter();
  @Output() onDelete = new EventEmitter();
  @Output() onTotal = new EventEmitter();
  @Output() onView = new EventEmitter();
  public  bookData: BooksModel[]=[];


  ngOnInit() {
    this.isAdmin=this.sharedDatabaseService.isAdmin;
    this.bookData=this.sharedDatabaseService.getBookList();
  }
  public deleteCategory() {
    this.sharedDatabaseService.deleteCategory(this.categoryData.catId);
    this.onDelete.emit();
  }
  public editCategory() {
   // this.categoriesDatabaseService.setCategoryToEdit(this.categoryData);
    this.onEdit.emit()

  }
  public viewBooksInCategory() {
    this.onView.emit()
  }
  public totalBooks() {
    this.onTotal.emit();
  }

}

