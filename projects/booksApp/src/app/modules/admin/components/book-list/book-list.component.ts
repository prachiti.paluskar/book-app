import { SharedDatabaseService } from './../../../shared/services/shared-database.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import _ from 'lodash';

@Component({
  selector: 'book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss'] 
})
export class BookListComponent implements OnInit {

  /*******************constructor********************************/
  constructor( public sharedDatabaseService: SharedDatabaseService,private router:Router) {
  }
  /********************Properties*******************************/
  public  bookList: BooksModel[] = []
  public filteredList: BooksModel[] = [];
  public categoryList:CategoryModel[] = [];
  public loginList:LoginModel[]=[];
  public currentUserData:LoginModel;
  public searchBookByCategory="";
  public searchBook="";
  public selectedCategoryList:[]=[];
  public books:number;
  public issue:number;
  public total:number;
  public isAdmin:string;
  public issued:any;
  /*****************************Methods**********************/
  ngOnInit() {
    this.books=this.sharedDatabaseService.bookList.length;
    this.getBookList();
    console.log("hi",this.books);
    this.isAdmin=this.sharedDatabaseService.isAdmin;
    this.categoryList=this.sharedDatabaseService.getCategoryList(); 
    //this.issued=this.sharedDatabaseService.issueBooksByUser();  
  }
  private getBookList()
  {
    this.bookList=this.sharedDatabaseService.getBookList();
    this.filteredList=_.cloneDeep(this.bookList);
    console.log('book-list',this.bookList);
    
  }
  public deleteData(index:number) {
   this.sharedDatabaseService.deleteData(this.bookList[index]._id);
   this.getBookList();
  }
  public issueBook(index:number) {
   this.sharedDatabaseService.issueBook(this.bookList[index]._id);
    this.getBookList();
  }
  public returnBook(index:number,userIndex:number) {
    this.sharedDatabaseService.returnBook(this.bookList[index]._id,this.currentUserData);
     this.getBookList();
  }
  public trackByIndex(index:any):any {
    return index;
  }
  public viewUsers(index) {
    this.sharedDatabaseService.issueBooksByUser();
    this.router.navigate(['/admin/userView/'+this.bookList[index]._id]);
  }
  public editData(index) {
    this.router.navigate(['admin/edit/'+this.bookList[index]._id]);
  } 
  public filterByName() {
    this.filteredList=this.bookList.filter((books)=>{
      if(books.bookName.toLowerCase().indexOf(this.searchBook.toLowerCase())!==-1)
      {
        return true;
      }
      return false;
    })
  }
  public filterByCategory() {
    this.filteredList=this.bookList.filter((categories)=>{
      for(let i=0;i<=this.selectedCategoryList.length;i++) {     
        if(categories.catIdList.indexOf(this.selectedCategoryList[i])!==-1 || this.selectedCategoryList.length===0) 
        {
          return true;
        }
      }
      return false;
    })
  }
}
