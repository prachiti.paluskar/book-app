import { ViewUsersComponent } from './components/view-users/view-users.component';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { AddEditBookComponent } from './components/add-edit-book/add-edit-book.component';
import { CategoryContainerComponent } from './components/category-container/category-container.component';
import { BookContainerComponent } from './components/book-container/book-container.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserLoginComponent } from '../shared/components/user-login/user-login.component';
import { AddEditCategoryComponent } from './components/add-edit-category/add-edit-category.component';

const routes: Routes = [
  {path:'',component: BookContainerComponent},
  {path:'login',component:UserLoginComponent},
  {path:'books',component:BookContainerComponent},
  {path:'category',component:CategoryContainerComponent},
  {path:'add',component:AddEditBookComponent},
  {path:'edit/:id',component:AddEditBookComponent},
  {path:'catAdd',component:AddEditCategoryComponent},
  {path:'catEdit/:id',component:AddEditCategoryComponent},
  {path:'booksPresent/:id',component:CategoryListComponent},
  {path:'userView/:id',component:ViewUsersComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
