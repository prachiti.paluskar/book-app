import { AddEditBookComponent } from './components/add-edit-book/add-edit-book.component';
import { SharedModule } from '../shared/shared.module';
import { MaterialModule } from '../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule } from '@angular/forms';
import { AddEditCategoryComponent } from './components/add-edit-category/add-edit-category.component';

@NgModule({
  declarations: [AddEditBookComponent,  AddEditCategoryComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MaterialModule,
    FormsModule,
    SharedModule,
  ],
  exports:[AddEditBookComponent,AddEditCategoryComponent]
})
export class AdminModule { }
