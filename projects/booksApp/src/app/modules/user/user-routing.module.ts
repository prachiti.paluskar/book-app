import { CategoryContainerComponent } from './../admin/components/category-container/category-container.component';
import { BookContainerComponent } from './../admin/components/book-container/book-container.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewBookComponent } from '../admin/components/view-book/view-book.component';

const routes: Routes = [
{path:'',component:BookContainerComponent},
{path:'category',component:CategoryContainerComponent},
{path:'view/:id',component:ViewBookComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
