interface CategoryModel {
    catId:string;
    categoryName:string;
    description:string;
    idList:[];
    imageUrl:string;
    bookPresent:string;
}