interface BooksModel {
    _id:string;
    bookName:string;
    description:string;
    bookImage:string;
    author:string;
    bookStatus:'ISSUED'|'NOT-ISSUED';
    catIdList:[];
    issuedBooks:number;
    totalBooks:number;
    availableBooks:number;
    bookIssuedBy:string[];
}